# -*- coding: utf-8 -*-

def builder(
    _context,
    _nameSpace,
    _module,
    _impl,
):
    packageTree = [
        'wscripts',
        _nameSpace,
        _module,
    ]
    if _impl is not None:
        packageTree.append( _impl )

    _builder(
        _context,
        _getPackageString( packageTree ),
    )

def _builder(
    _context,
    _package,
):
    exec(
'''
from %(package)s import build
build(
    _context,
)
''' % {
    'package' : _package,
}
    )

def _getPackageString(
    _packageTree,
):
    return '.'.join( _packageTree )
