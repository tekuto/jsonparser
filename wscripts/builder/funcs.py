# -*- coding: utf-8 -*-

import os.path

def buildProgram(
    _context,
    _target,
    source = dict(),
    use = list(),
    lib = list(),
    stlib = list(),
):
    _build(
        _context.program,
        _context,
        _target,
        source,
        use,
        lib,
        stlib,
    )

def buildShlib(
    _context,
    _target,
    source = dict(),
    use = list(),
    lib = list(),
    stlib = list(),
):
    _build(
        _context.shlib,
        _context,
        _target,
        source,
        use,
        lib,
        stlib,
    )

def buildStlib(
    _context,
    _target,
    source = dict(),
    use = list(),
    lib = list(),
    stlib = list(),
):
    _build(
        _context.stlib,
        _context,
        _target,
        source,
        use,
        lib,
        stlib,
    )

def _build(
    _buildFunc,
    _context,
    _target,
    _source,
    _use,
    _lib,
    _stlib,
):
    _buildFunc(
        target = _target,
        source = _generateSources( _source ),
        use = _use,
        lib = _lib,
        stlib = _stlib,
        includes = _context.env.MY_INCLUDES,
        libpath = _context.env.MY_LIBPATH,
        stlibpath = _context.env.MY_STLIBPATH,
        defines = _context.env.MY_DEFINES,
        cxxflags = _context.env.MY_CXXFLAGS,
        linkflags = _context.env.MY_LINKFLAGS,
    )

def _generateSources(
    _sources,
    _parent = None,
):
    result = list()

    TYPE = type( _sources )
    if TYPE is dict:
        for key, values in _sources.items():
            parent = key
            if _parent is not None:
                parent = os.path.join(
                    _parent,
                    parent,
                )

            result += _generateSources(
                values,
                parent,
            )
    elif TYPE is set or TYPE is list:
        for i in _sources:
            result += _generateSources(
                i,
                _parent,
            )
    else:
        if _parent is not None:
            _sources = os.path.join(
                _parent,
                _sources,
            )

        result.append( _sources )

    return result
