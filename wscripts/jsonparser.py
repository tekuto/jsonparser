# -*- coding: utf-8 -*-

from . import common
from .builder.funcs import buildProgram

def build( _context ):
    TARGET = common.JSONPARSER

    SOURCE = {
        common.SOURCE_DIR : {
            TARGET : {
                'main.cpp',
            },
        },
    }

    buildProgram(
        _context,
        TARGET,
        source = SOURCE,
    )
