﻿#include <string>
#include <vector>
#include <map>
#include <memory>
#include <cstring>
#include <utility>
#include <cstdio>

struct Token
{
    size_t  position;
    size_t  length;
};

typedef std::vector< Token > Tokens;

enum JsonType
{
    INVALID = 0x0,

    STRING,
    ARRAY,
    OBJECT,
};

struct JsonString
{
    std::string value;

    JsonString(
        const std::string & _STRING
    )
        : value( _STRING )
    {
    }
};

struct Json;

typedef std::vector< std::unique_ptr< Json > > Array;

struct JsonArray
{
    Array   value;

    JsonArray(
        Array &&    _value
    )
        : value( std::move( _value ) )
    {
    }
};

typedef std::map< std::string, std::unique_ptr< Json > > Object;

struct JsonObject
{
    Object  value;

    JsonObject(
        Object &&   _value
    )
        : value( std::move( _value ) )
    {
    }
};

struct Json
{
    JsonType    type;

    union {
        JsonString *    str;
        JsonArray *     array;
        JsonObject *    object;
    };

    Json(
        const std::string & _STRING
    )
        : type( STRING )
        , str( new JsonString( _STRING ) )
    {
    }

    Json(
        Array &&    _array
    )
        : type( ARRAY )
        , array( new JsonArray( std::move( _array ) ) )
    {
    }

    Json(
        Object &&   _object
    )
        : type( OBJECT )
        , object( new JsonObject( std::move( _object ) ) )
    {
    }

    ~Json(
    )
    {
        switch( this->type ) {
        case STRING:
            delete this->str;
            break;

        case ARRAY:
            delete this->array;
            break;

        case OBJECT:
            delete this->object;
            break;
        }
    }
};

bool readString(
    std::string &   _string
)
{
    while( 1 ) {
        const auto  CH = std::fgetc( stdin );
        if( CH == EOF ) {
            break;
        }

        _string.push_back( CH );
    }

    return true;
}

bool isStringSeparatorToken(
    char    _ch
)
{
    return _ch == '"';
}

const std::string   SINGLE_TOKENS = "{}[],:";

bool isSingleToken(
    char    _ch
)
{
    return SINGLE_TOKENS.find_first_of( _ch ) != std::string::npos;
}

bool isSpace(
    char    _ch
)
{
    return std::isspace( _ch ) != 0;
}

bool isEscape(
    char    _ch
)
{
    return _ch == '\\';
}

void addToken(
    Tokens &    _tokens
    , Token &   _token
)
{
    if( _token.length <= 0 ) {
        return;
    }

    _tokens.push_back( _token );

    _token.position += _token.length;
    _token.length = 0;
}

void addSingleToken(
    Tokens &    _tokens
    , Token &   _token
)
{
    addToken(
        _tokens
        , _token
    );

    _token.length++;
    addToken(
        _tokens
        , _token
    );
}

bool toTokens(
    Tokens &                _tokens
    , const std::string &   _STRING
)
{
    bool    inString = false;
    bool    escaped = false;
    Token   token = { 0, 0 };

    for( const auto & CH : _STRING ) {
        if( inString == false ) {
            if( isStringSeparatorToken( CH ) ) {
                addSingleToken(
                    _tokens
                    , token
                );

                inString = true;

                continue;
            } else if( isSingleToken( CH ) ) {
                addSingleToken(
                    _tokens
                    , token
                );

                continue;
            } else if( isSpace( CH ) ) {
                addToken(
                    _tokens
                    , token
                );
                token.position++;

                continue;
            }
        } else {
            if( escaped == false ) {
                if( isStringSeparatorToken( CH ) ) {
                    addSingleToken(
                        _tokens
                        , token
                    );

                    inString = false;

                    continue;
                } else if( isEscape( CH ) ) {
                    escaped = true;
                }
            } else {
                escaped = false;
            }
        }

        token.length++;
    }

    if( inString == true ) {
        std::printf( "%lld文字目から始まる文字列が閉じていない\n", token.position );

        return false;
    }

    return true;
}

void writeTokens(
    const Tokens &          _TOKENS
    , const std::string &   _STRING
)
{
    for( const auto & TOKEN : _TOKENS ) {
        std::printf( "%.*s\n", TOKEN.length, &( _STRING[ TOKEN.position ] ) );
    }
}

bool isStringSeparator(
    const Token &           _TOKEN
    , const std::string &   _STRING
)
{
    return _STRING.compare(
        _TOKEN.position
        , _TOKEN.length
        , "\""
    ) == 0;
}

bool isArrayBegin(
    const Token &           _TOKEN
    , const std::string &   _STRING
)
{
    return _STRING.compare(
        _TOKEN.position
        , _TOKEN.length
        , "["
    ) == 0;
}

bool isArraySeparator(
    const Token &           _TOKEN
    , const std::string &   _STRING
)
{
    return _STRING.compare(
        _TOKEN.position
        , _TOKEN.length
        , ","
    ) == 0;
}

bool isArrayEnd(
    const Token &           _TOKEN
    , const std::string &   _STRING
)
{
    return _STRING.compare(
        _TOKEN.position
        , _TOKEN.length
        , "]"
    ) == 0;
}

bool isObjectBegin(
    const Token &           _TOKEN
    , const std::string &   _STRING
)
{
    return _STRING.compare(
        _TOKEN.position
        , _TOKEN.length
        , "{"
    ) == 0;
}

bool isObjectPairSeparator(
    const Token &           _TOKEN
    , const std::string &   _STRING
)
{
    return _STRING.compare(
        _TOKEN.position
        , _TOKEN.length
        , ":"
    ) == 0;
}

bool isObjectSeparator(
    const Token &           _TOKEN
    , const std::string &   _STRING
)
{
    return _STRING.compare(
        _TOKEN.position
        , _TOKEN.length
        , ","
    ) == 0;
}

bool isObjectEnd(
    const Token &           _TOKEN
    , const std::string &   _STRING
)
{
    return _STRING.compare(
        _TOKEN.position
        , _TOKEN.length
        , "}"
    ) == 0;
}

void fromEscapedQuotation(
    std::string &   _str
)
{
    size_t  position = 0;
    while( 1 ) {
        position = _str.find( "\\\"", position );
        if( position == std::string::npos ) {
            break;
        }

        _str.replace(
            position
            , 2
            , "\""
        );
    }
}

void fromEscapedYenSign(
    std::string &   _str
)
{
    size_t  position = 0;
    while( 1 ) {
        position = _str.find( "\\\\", position );
        if( position == std::string::npos ) {
            break;
        }

        _str.replace(
            position
            , 2
            , "\\"
        );
    }
}

void fromEscaped(
    std::string &   _str
)
{
    fromEscapedQuotation( _str );
    fromEscapedYenSign( _str );
}

void escapeYenSign(
    std::string &   _str
)
{
    size_t  position = 0;
    while( 1 ) {
        position = _str.find( "\\", position );
        if( position == std::string::npos ) {
            break;
        }

        _str.replace(
            position
            , 1
            , "\\\\"
        );
        position += 2;
    }
}

void escapeQuotation(
    std::string &   _str
)
{
    size_t  position = 0;
    while( 1 ) {
        position = _str.find( "\"", position );
        if( position == std::string::npos ) {
            break;
        }

        _str.replace(
            position
            , 1
            , "\\\""
        );
        position += 2;
    }
}

void escape(
    std::string &   _str
)
{
    escapeYenSign( _str );
    escapeQuotation( _str );
}

Json * newJsonString(
    Tokens::const_iterator &            _it
    , const Tokens::const_iterator &    _END
    , const std::string &               _STRING
)
{
    if( _it == _END ) {
        return nullptr;
    }

    if( isStringSeparator(
        *_it
        , _STRING
    ) == false ) {
        return nullptr;
    }

    ++_it;
    if( _it == _END ) {
        return nullptr;
    }

    std::string str;
    if( isStringSeparator(
        *_it
        , _STRING
    ) == false ) {
        const auto &    TOKEN = *_it;

        ++_it;
        if( _it == _END ) {
            return nullptr;
        }
        if( isStringSeparator(
            *_it
            , _STRING
        ) == false ) {
            return nullptr;
        }

        str.assign(
            _STRING
            , TOKEN.position
            , TOKEN.length
        );

        fromEscaped( str );
    }

    ++_it;

    return new Json( str );
}

Json * newJson(
    Tokens::const_iterator &
    , const Tokens::const_iterator &
    , const std::string &
);

Json * newJsonArray(
    Tokens::const_iterator &            _it
    , const Tokens::const_iterator &    _END
    , const std::string &               _STRING
)
{
    if( _it == _END ) {
        return nullptr;
    }

    if( isArrayBegin(
        *_it
        , _STRING
    ) == false ) {
        return nullptr;
    }

    ++_it;
    if( _it == _END ) {
        return nullptr;
    }

    Array   array;
    while( 1 ) {
        std::unique_ptr< Json > jsonUnique(
            newJson(
                _it
                , _END
                , _STRING
            )
        );
        if( jsonUnique.get() == nullptr ) {
            return nullptr;
        }

        array.push_back( std::move( jsonUnique ) );

        if( _it == _END ) {
            return nullptr;
        } else if( isArraySeparator(
            *_it
            , _STRING
        ) ) {
            ++_it;
            continue;
        } else if( isArrayEnd(
            *_it
            , _STRING
        ) ) {
            ++_it;
            break;
        } else {
            return nullptr;
        }
    }

    return new Json( std::move( array ) );
}

bool getJsonObjectKey(
    std::string &                       _key
    , Tokens::const_iterator &          _it
    , const Tokens::const_iterator &    _END
    , const std::string &               _STRING
)
{
    std::unique_ptr< Json > keyUnique(
        newJsonString(
            _it
            , _END
            , _STRING
        )
    );
    if( keyUnique.get() == nullptr ) {
        return false;
    }
    auto &  key = *keyUnique;

    _key.assign( std::move( key.str->value ) );

    return true;
}

void insertPair(
    Object &                        _object
    , std::string &&                _key
    , std::unique_ptr< Json > &&    _jsonUnique
)
{
    auto    it = _object.find( _key );
    if( it != _object.end() ) {
        _object.erase( it );
    }

    _object.insert(
        Object::value_type(
            std::move( _key )
            , std::move( _jsonUnique )
        )
    );
}

Json * newJsonObject(
    Tokens::const_iterator &            _it
    , const Tokens::const_iterator &    _END
    , const std::string &               _STRING
)
{
    if( _it == _END ) {
        return nullptr;
    }

    if( isObjectBegin(
        *_it
        , _STRING
    ) == false ) {
        return nullptr;
    }

    ++_it;
    if( _it == _END ) {
        return nullptr;
    }

    Object  object;

    while( 1 ) {
        std::string key;
        if( getJsonObjectKey(
            key
            , _it
            , _END
            , _STRING
        ) == false ) {
            return nullptr;
        }

        if( _it == _END ) {
            return nullptr;
        } else if( isObjectPairSeparator(
            *_it
            , _STRING
        ) == false ) {
            return nullptr;
        }
        ++_it;

        std::unique_ptr< Json > jsonUnique(
            newJson(
                _it
                , _END
                , _STRING
            )
        );
        if( jsonUnique.get() == nullptr ) {
            return nullptr;
        }

        insertPair(
            object
            , std::move( key )
            , std::move( jsonUnique )
        );

        if( _it == _END ) {
            return nullptr;
        } else if( isObjectSeparator(
            *_it
            , _STRING
        ) ) {
            ++_it;
            continue;
        } else if( isObjectEnd(
            *_it
            , _STRING
        ) ) {
            break;
        } else {
            return nullptr;
        }
    }

    return new Json( std::move( object ) );
}

Json * newJson(
    Tokens::const_iterator &            _it
    , const Tokens::const_iterator &    _END
    , const std::string &               _STRING
)
{
    if( _it == _END ) {
        return nullptr;
    }

    std::unique_ptr< Json > jsonUnique;

    const auto &    TOKEN = *_it;
    if( isStringSeparator(
        TOKEN
        , _STRING
    ) ) {
        jsonUnique.reset(
            newJsonString(
                _it
                , _END
                , _STRING
            )
        );
    } else if( isArrayBegin(
        TOKEN
        , _STRING
    ) ) {
        jsonUnique.reset(
            newJsonArray(
                _it
                , _END
                , _STRING
            )
        );
    } else if( isObjectBegin(
        TOKEN
        , _STRING
    ) ) {
        jsonUnique.reset(
            newJsonObject(
                _it
                , _END
                , _STRING
            )
        );
    }
    //TODO

    return jsonUnique.release();
}

Json * newJsonRoot(
    Tokens::const_iterator &            _it
    , const Tokens::const_iterator &    _END
    , const std::string &               _STRING
)
{
    std::unique_ptr< Json > jsonUnique(
        newJson(
            _it
            , _END
            , _STRING
        )
    );
    if( jsonUnique.get() == nullptr ) {
        return nullptr;
    }

    if( _it != _END ) {
        const auto &    TOKEN = *_it;

        std::printf( "%llu文字目から始まる文字列がJSONとして不正\n", TOKEN.position );

        return nullptr;
    }

    return jsonUnique.release();
}

void writeJsonStringValue(
    const JsonString &  _STRING
)
{
    std::string escaped = _STRING.value;
    escape( escaped );

    std::printf( "\"%s\"", escaped.c_str() );
}

void writeJson(
    const Json &
);

void writeJsonArrayValue(
    const JsonArray &   _ARRAY
)
{
    std::printf( "[ " );

    const auto &    ARRAY = _ARRAY.value;

    auto        it = ARRAY.begin();
    const auto  END = ARRAY.end();
    while( 1 ) {
        const auto &    JSON = **it;

        writeJson( JSON );

        ++it;
        if( it == END ) {
            break;
        }

        std::printf( ", " );
    }

    std::printf( " ]" );
}

void writeJsonObjectPair(
    const Object::value_type &  _PAIR
)
{
    std::printf( "\"%s\" : ", _PAIR.first.c_str() );
    writeJson( *( _PAIR.second ) );
}

void writeJsonObjectValue(
    const JsonObject &  _OBJECT
)
{
    std::printf( "{ " );

    const auto &    OBJECT = _OBJECT.value;

    auto        it = OBJECT.begin();
    const auto  END = OBJECT.end();
    while( 1 ) {
        const auto &    PAIR = *it;

        writeJsonObjectPair(
            PAIR
        );

        ++it;
        if( it == END ) {
            break;
        }

        std::printf( ", " );
    }

    std::printf( " }" );
}

void writeJson(
    const Json &    _JSON
)
{
    std::printf( "{ " );
    std::printf( "\"type\" : \"" );
    switch( _JSON.type ) {
    case STRING:
        std::printf( "string" );
        break;

    case ARRAY:
        std::printf( "array" );
        break;

    case OBJECT:
        std::printf( "object" );
        //TODO
    }

    std::printf( "\", " );
    std::printf( "\"value\" : " );
    switch( _JSON.type ) {
    case STRING:
        writeJsonStringValue(
            *( _JSON.str )
        );
        break;

    case ARRAY:
        writeJsonArrayValue(
            *( _JSON.array )
        );
        break;

    case OBJECT:
        writeJsonObjectValue(
            *( _JSON.object )
        );
        break;

        //TODO
    }

    std::printf( " }" );
}

void writeJsonRoot(
    const Json &    _JSON
)
{
    writeJson(
        _JSON
    );

    std::printf( "\n" );
}

int main(
)
{
    std::string str;
    if( readString(
        str
    ) == false ) {
        return 1;
    }

    Tokens  tokens;
    if( toTokens(
        tokens
        , str
    ) == false ) {
        return 1;
    }

    writeTokens(
        tokens
        , str
    );

    Tokens::const_iterator          it = tokens.begin();
    const Tokens::const_iterator    TOKENS_END = tokens.end();

    std::unique_ptr< Json > jsonUnique(
        newJson(
            it
            , TOKENS_END
            , str
        )
    );
    if( jsonUnique.get() == nullptr ) {
        return 1;
    }
    const auto &    JSON = *jsonUnique;

    writeJsonRoot(
        JSON
    );

    return 0;
}
